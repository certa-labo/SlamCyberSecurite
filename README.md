# Thèmes abordables en SIO SLAM Application Web et Cybersécurité 

Liste construite d'après des manquements en cybersécurité identifiés sur des productions d'apprentis développeur, de une à deux années d'expérience en développement d'applications web.

|Thèmes à aborder | Documentations en ligne | Ressources pédagogiques | Évaluations (QCM, ...)
|--------------- |--------------- |--------------- | ---------------
|Différentes techniques de suivis de session HTTP| | |
|Qualité de la structure du contenu retourné au client (didactique : passer par XML ? Bien formé et valide?) | | |
|La redirection côté client : définition et différentes formes de réalisation | | |
|Pattern PRG généralisé et message Flash |- [Post-Rediret-Get (wikipedia)](https://fr.wikipedia.org/wiki/Post-redirect-get) <br /> - TODO "Flash Message" def   | |
|Respect de conventions de nommage et d’organisation des fichiers (capacité à s’adapter) | | |
|Niveau de confiance des données d’entrée  | | |
|Identification de la surface d’attaque (définition de périmètres, modélisation) | | |
|Stratégie de gestion de la surface d’attaque (réduction, gestion, délégation) | | |
|Mot de passe chiffrés en base de données et comment l’utiliser | | |
|Identité utilisée par l’application pour se connecter au système de persistance | | |
|Bon usage de .gitignore| | |
| | | |
| | | |

